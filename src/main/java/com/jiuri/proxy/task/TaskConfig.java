package com.jiuri.proxy.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

/**
 * @program: proxy-pool
 * @author: Mr.Han
 * @create: 2019-03-05 17:59
 **/

/**
 * 设置定时任务线程池最大数为5，默认为单一阻塞，效率太差
 */
@Configuration
public class TaskConfig implements SchedulingConfigurer {
    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.setScheduler(Executors.newScheduledThreadPool(5));
    }
}
