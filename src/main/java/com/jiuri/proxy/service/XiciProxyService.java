package com.jiuri.proxy.service;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.jiuri.proxy.async.CheckProxyAsync;
import com.jiuri.proxy.config.XiciConfig;
import com.jiuri.proxy.dao.ProxyDAO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-25 17:51
 **/
@Service
public class XiciProxyService {
    private static Logger LOGGER = LoggerFactory.getLogger(XiciProxyService.class);
    @Autowired
    private XiciConfig xiciConfig;
    @Autowired
    private CheckProxyAsync checkProxyAsync;
    @Autowired
    private ProxyDAO proxyDAO;
    @Async
    public void xiciProxy(String url, String proxy) {
        HttpResponse response;
        try {
            response = HttpUtil.createGet(url).setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(splitProxy(proxy)[0], Integer.parseInt(splitProxy(proxy)[1])))).timeout(xiciConfig.getTimeout()).execute();
        } catch (RuntimeException e) {
            xiciProxy(url, proxyDAO.randomGetHighAvailableProxy());
            return;
        } catch (Exception e){
            LOGGER.info("西刺代理请求失败,url:{}",url);
            return;
        }
        if (!response.isOk()) {
            xiciProxy(url, proxyDAO.randomGetHighAvailableProxy());
            return;
        }
        Document document = Jsoup.parse(response.body());
        Elements table = document.select("table tr");
        table.remove(0);
        for (Element element : table) {
            String ip = element.select("td:nth-child(2)").text();
            Integer port = Integer.parseInt(element.select("td:nth-child(3)").text());
            checkProxyAsync.check(ip, port);
        }
        String next_page = document.select(".next_page").attr("href");
        Integer pageNum = getPageNum(next_page);
        if(xiciConfig.getEndPageNum()==pageNum){
            LOGGER.info("西刺 --- end");
            return;
        }
        LOGGER.info("西刺代理 --- get page : "+url.split("/n")[0]+next_page);
        xiciProxy(url.split("/n")[0] + next_page, proxy);

    }
    //获取下一页路径
    public Integer getPageNum(String next_page) {
        if (next_page.contains("/nn/")) {
            return Integer.parseInt(next_page.replaceAll("/nn/", ""));
        }
        if (next_page.contains("/nt/")) {
            return Integer.parseInt(next_page.replaceAll("/nt/", ""));
        }
        return null;
    }
    //
    public String[] splitProxy(String proxy) {
        return new String[]{proxy.split(":")[0], proxy.split(":")[1]};
    }
}
