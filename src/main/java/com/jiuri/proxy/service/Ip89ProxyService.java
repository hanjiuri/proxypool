package com.jiuri.proxy.service;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.jiuri.proxy.async.CheckProxyAsync;
import com.jiuri.proxy.config.Ip89Config;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @program: proxy
 * @author: Mr.Han
 * @create: 2018-12-25 17:51
 **/
@Service
public class Ip89ProxyService{
    private static Logger LOGGER = LoggerFactory.getLogger(Ip89ProxyService.class);
    @Autowired
    private Ip89Config ip89Config;
    @Autowired
    private CheckProxyAsync checkProxyAsync;
    @Async
    public void ip89Proxy(String url) {
        HttpResponse execute;
        try {
            execute = HttpUtil.createGet(url).timeout(ip89Config.getTimeout()).execute();
        }catch (Exception e){
            LOGGER.info("ip89请求失败，url:{}",url);
            return;
        }
        if(!execute.isOk()){
            LOGGER.info("访问"+url+"失败");
            return;
        }
        Document parse = Jsoup.parse(execute.body());
        Elements elements = parse.select("table tr");
        elements.remove(0);
        for (Element element : elements) {
            String ip = element.select("td:nth-child(1)").text().replaceAll(" ","");
            Integer port = Integer.parseInt(element.select("td:nth-child(2)").text().replaceAll(" ",""));
            checkProxyAsync.check(ip,port);
        }
        String href = parse.select("div a").select(".layui-laypage-next").attr("href").replaceAll("index_","").replaceAll(".html","");
        Integer nextPageNum = Integer.parseInt(href);
        if(nextPageNum==ip89Config.getEndPageNum()){
            LOGGER.info("ip89 --- end");
            return;
        }
        String nextPage = url.split("/index")[0]+"/index_"+nextPageNum+".html";
        LOGGER.info("89ip --- get page : "+nextPage);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ip89Proxy(nextPage);
    }

}
